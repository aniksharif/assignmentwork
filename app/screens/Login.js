import React from 'react';
import { TouchableOpacity,Platform, StyleSheet, Text, View, ActivityIndicator, StatusBar, Image, TextInput } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';

export default class LoginScreen extends React.PureComponent {
  constructor(props) {
    super(props);


  }
  render() {
    return (

      <View style={styles.container}>
        <View>
          <Image
            style={styles.logo}
            source={require('../images/logo.png')}
            resizeMode={'center'}
          />
        </View>
        <View style={styles.passwordContainer}>
        <View style={{padding:10}}>
        <FontAwesome
            name="user-o"
            color='#CF6464'
            size={25}
          />
          </View>
          <TextInput
            style={styles.inputStyle}
            autoCorrect={false}
            secureTextEntry
            placeholder="USERNAME"
            
          />
          
        </View>
        <View style={styles.passwordContainer}>
        <View style={{padding:10}}>
        <SimpleLineIcons
            name="lock"
            color='#CF6464'
            size={25}
          />
          </View>
          <TextInput
            style={styles.inputStyle}
            autoCorrect={false}
            placeholder="▪ ▪ ▪ ▪ ▪ ▪ ▪ ▪"
            
          />
          
        </View>
        <TouchableOpacity style={styles.login} onPress={()=>this.props.navigation.navigate("Home")} >
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
      </View>


    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  passwordContainer: {
    flexDirection: 'row',
    borderWidth: 1,
    width:'70%',
    borderColor: '#D0D0D0',
    borderRadius:5,
    margin:10
  },
  inputStyle: {
    flex: 1,
    width:'40%',
    borderLeftWidth:1,
    backgroundColor: '#D0D0D0',
    borderColor: '#D0D0D0',
  },
  logo: {
    justifyContent: 'center',
    bottom: 80,
    padding: 10,
    width: 100,
    height: 100
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  login: {
    backgroundColor:'#FF3333',
    borderRadius:5,
    width:'45%',
    height:55,
  },
  loginText:{
    color:'white',
    fontSize:20,
    textAlign:'center',
    padding:10

  }
});