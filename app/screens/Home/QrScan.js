import React from 'react';
import { TouchableOpacity, Platform, StyleSheet, Text, View, ActivityIndicator, Dimensions, Image, TextInput } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';
import QRCodeScanner from 'react-native-qrcode-scanner';

const SCREEN_HEIGHT = Dimensions.get("window").height;
const SCREEN_WIDTH = Dimensions.get("window").width;
export default class QrScan extends React.PureComponent {
    constructor(props) {
        super(props);


    }
    onSuccess(e) {

        var temp = e.data;
        alert(temp)
        this.props.navigation.navigate('Payment')
    }
  

    render() {
        return (

            <QRCodeScanner
                onRead={this.onSuccess.bind(this)}
                showMarker
                cameraStyle={{ height: SCREEN_HEIGHT }}
                customMarker={
                    <View style={styles.rectangleContainer}>
                        <View style={styles.topOverlay}>
                          <TouchableOpacity onPress={()=> this.props.navigation.navigate('Payment')}>
                              <Text style={{fontSize:20,fontWeight:'bold',color:'red'}}>Next</Text>
                          </TouchableOpacity>
                        </View>

                        <View style={{ flexDirection: "row" }}>
                            <View style={styles.leftAndRightOverlay} />

                            <View style={styles.rectangle}>
                                
                            </View>

                            <View style={styles.leftAndRightOverlay} />
                        </View>

                        <View style={styles.bottomOverlay} />
                    </View>
                }

            />





        );
    }
}
const overlayColor = "rgba(0,0,0,0.50)"; 

const rectDimensions = SCREEN_WIDTH * 0.75; 
const rectBorderWidth = 5; 
const rectBorderColor = "red";

const scanBarWidth = SCREEN_WIDTH * 0.46; 
const scanBarHeight = SCREEN_WIDTH * 0.0025; 
const scanBarColor = "#22ff00";


const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    rectangleContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },

    rectangle: {
        height: rectDimensions,
        width: rectDimensions,
        borderWidth: rectBorderWidth,
        borderColor: rectBorderColor,
        borderRadius:10,
        overflow:"hidden",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: "transparent"
    },

    topOverlay: {
        flex: 1,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor,
        justifyContent: "center",
        alignItems: "center"
    },

    bottomOverlay: {
        flex: 1,
        height: SCREEN_WIDTH,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor,
        paddingBottom: SCREEN_WIDTH * 0.50
    },

    leftAndRightOverlay: {
        height: SCREEN_WIDTH * 0.75,
        width: SCREEN_WIDTH,
        backgroundColor: overlayColor
    },

    scanBar: {
        width: scanBarWidth,
        height: scanBarHeight,
        backgroundColor: scanBarColor
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    passwordContainer: {
        flexDirection: 'row',
        borderWidth: 1,
        width: '60%',
        borderColor: '#D0D0D0',
        borderRadius: 5,
        margin: 10
    },
    inputStyle: {
        flex: 1,
        width: '40%',
        borderLeftWidth: 1,
        backgroundColor: '#D0D0D0',
        borderColor: '#D0D0D0',
    },
    logo: {
        justifyContent: 'center',
        bottom: 80,
        padding: 10,
        width: 100,
        height: 100
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    login: {
        backgroundColor: '#FF3333',
        borderRadius: 5,
        padding: 10,
        margin: 10,
        color: 'white',
        fontSize: 20
    }
});