import React from 'react';
import { TouchableOpacity, Platform, StyleSheet, Text, View, ActivityIndicator, StatusBar, Image, TextInput } from 'react-native';




export default class Page2 extends React.PureComponent {
  constructor(props) {
    super(props);
    

  }
  render() {
    return (

      <View style={styles.container}>

        <View>
          <Image
            style={styles.profile}
            source={require('../../images/moneyHand.png')}
            resizeMode={'cover'}
          />
        </View>

      

        <View style={styles.rulesContainer}>
          <Text style={styles.rules}>
            {"You've Successfully"+'\n'+"Recharged 100 Taka"+'\n'+"to +88....."}
          </Text>

        </View>


        <View >
          <TouchableOpacity style={styles.buttons} onPress={()=>this.props.navigation.navigate("Home")}>
            <Text style={styles.buttonText}>Home</Text>
          </TouchableOpacity>
        </View>

      </View>


    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  profile: {
    justifyContent: 'center',
    width: 100,
    height: 100,
    padding: 10,
    borderRadius: 10
  },

  buttons: {
    backgroundColor:'#FF3333',
    borderRadius:5,
    width:'45%',
    height:55,
  },

  buttonText: {
    color:'white',
    fontSize:20,
    textAlign:'center',
    padding:10
  },
  rulesContainer: {
    padding: 30
  },
  rules: {
    color: '#FE3232',
    fontSize: 15,
    textAlign: 'center',

  }

});