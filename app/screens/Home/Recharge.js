import React from 'react';
import { TouchableOpacity, ScrollView, StyleSheet, Text, View, ActivityIndicator, StatusBar, Image, TextInput } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';




export default class Recharge extends React.PureComponent {
  constructor(props) {
    super(props);

  }
  render() {
    return (

      <View style={styles.container}>
        <ScrollView style={{top:'10%'}}>
          <View style={{
            justifyContent: 'center',
            alignItems: 'center',
            padding:10,
            
          }}>
            <View style={{ borderColor: '#FE3232', borderWidth: 3, borderRadius: 15 }}>
              <Image
                style={styles.profile}
                source={require('../../images/profilepic.png')}
                resizeMode={'cover'}
              />
            </View>

            <View style={{ alignItems: 'center', }}>
              <Text style={{ color: '#FE3232', fontSize: 15, }}>Aniruddha Chakraborty</Text>
              <Text style={{ color: '#FE3232', fontSize: 15, }}>+8801741487027</Text>
            </View>

            <View style={styles.rulesContainer}>
              <Text style={styles.rules}>Recharge Amount</Text>
              <Text style={styles.rules}>Your Current Balance</Text>
              <Text style={styles.rules}>Balance After Recharge</Text>
            </View>

            <View style={{ height: '20%' }}>

              <CodeInput
                ref="codeInputRef1"
                className={'border-b'}
                keyboardType={'numeric'}
                space={5}
                codeLength={4}
                size={60}
                inputPosition='left'
                inactiveColor='#D93232'
                activeColor='#D93232'
                cellBorderWidth={3}
                codeInputStyle={{ color: 'black', fontSize: 20 }}
              />

            </View>

            <View style={{padding:20}}>
              <TouchableOpacity style={styles.buttons} onPress={()=>this.props.navigation.navigate('Success')}>
                <Text style={styles.buttonText}>Recharge</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>

      </View>


    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  profile: {
    justifyContent: 'center',
    width: 100,
    height: 100,
    padding: 10,
    borderRadius: 10
  },

  buttons: {
    backgroundColor: '#FE3232',
    width: '40%',
    textAlign: 'center',
    height: 55,
    margin: 5,
    padding: 15,
    borderRadius: 5,
  },

  buttonText: {
    color: 'white',
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 15
  },
  rulesContainer: {
    textAlign: 'center',
    padding: 30
  },
  rules: {
    color: '#FE3232',
    fontSize: 15,
    textAlign: 'center',

  }

});