import React from 'react';
import { TouchableOpacity, Platform, StyleSheet, Text, View, ActivityIndicator, StatusBar, Image, TextInput } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Slider } from 'react-native-elements'



export default class Payment extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            value: 30
        }

    }
    render() {
        return (

            <View style={styles.container}>

                <View style={{ borderColor: '#FE3232', borderWidth: 3, borderRadius: 15 }}>
                    <Image
                        style={styles.profile}
                        source={require('../../images/profile.jpg')}
                        resizeMode={'cover'}
                    />
                </View>

                <View style={{ alignItems: 'center', }}>
                    <Text style={{ color: '#FE3232', fontSize: 15, }}>Aniruddha Chakraborty</Text>
                    <Text style={{ color: '#FE3232', fontSize: 15, }}>+8801741487027</Text>
                </View>

                <View >
                    <Image
                        style={{ width: 50, height: 50 }}
                        source={require('../../images/taka.png')}
                        resizeMode={'cover'}
                    />
                </View>

                <View style={styles.amountBoxContainer}>
                    <TouchableOpacity style={styles.amountBox}>
                        <Text style={styles.amountText}>20</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.amountBox}>
                        <Text style={styles.amountText}>30</Text>
                    </TouchableOpacity>
                </View>
                <View style={styles.amountBoxContainer}>
                    <TouchableOpacity style={styles.amountBox}>
                        <Text style={styles.amountText}>50</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.amountBox}>
                        <Text style={styles.amountText}>100</Text>
                    </TouchableOpacity>
                </View>

                <Text style={{fontWeight:'bold'}}>OR</Text>

                <View style={styles.sliderStyle}>
                <Text style={{textAlign:'center',fontWeight:'bold',fontSize:15, paddingTop:10}}>{this.state.value}</Text>
                    <Slider
                        maximumValue={100}
                        step={1}
                        minimumValue={0}
                        value={this.state.value}
                        onValueChange={(value) => this.setState({ value })} />
                    
                </View>

                <View style={{width:'100%', justifyContent:'center', alignItems:'center'}}>
                <TouchableOpacity style={styles.buttons}>
                    <Text style={styles.buttonText}>Amount</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.buttons} onPress={()=>this.props.navigation.navigate('Recharge')}>
                    <Text style={styles.buttonText}>Next</Text>
                </TouchableOpacity>
                </View>

            </View>


        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#ffffff',
    },
    profile: {
        justifyContent: 'center',
        width: 100,
        height: 100,
        padding: 10,
        borderRadius: 10
    },
    amountBox: {
        backgroundColor: '#D1D1D1',
        margin: 10,
        width: '30%',
        height: 50,
        padding: 10,
        borderRadius: 10,

    },
    amountText: {
        fontWeight: 'bold',
        fontSize: 20,
        textAlign: 'center',
    },
    amountBoxContainer: {
        flexDirection: 'row',
        overflow: 'hidden'
    },
    sliderStyle:{
        flex: 1, 
        alignItems: 'stretch',
        width:'50%', 
        justifyContent: 'center',
       
    },
    buttons: {
        backgroundColor: '#FE3232',
        fontSize: 15,
        margin: 5,
        width: '40%',
        textAlign: 'center',
        height: 40,
        padding: 10,
        borderRadius: 5,
    },
    buttonText: {
        color: 'white',
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: 15
    }

});