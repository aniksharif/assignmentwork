import React from 'react';
import { TouchableOpacity,Platform, StyleSheet, Text, View, ActivityIndicator, StatusBar, Image, TextInput } from 'react-native';
import SimpleLineIcons from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { Input } from 'react-native-elements';


export default class Home extends React.PureComponent {
  constructor(props) {
    super(props);


  }
  render() {
    return (

      <View style={styles.container}>
        
         <View style={{ bottom: 80,}}>
          <Image
          style={styles.profile}
          source={require('../../images/profilepic.png')}
          resizeMode={'cover'}
          />
          
        </View>

        <View style={{position:'absolute', top:80}}>
        <Image
          style={styles.profile}
          source={require('../../images/profileFrame.png')}
          resizeMode={'cover'}
          />
          </View> 
        
        <View style={{alignItems:'center', bottom:60, }}>
          <Text style={{color:'#FE3232', fontSize: 15,}}>Aniruddha Chakraborty</Text>
          <Text style={{color:'#FE3232', fontSize: 15,}}>1000 Tk</Text>
        </View>

        <TouchableOpacity onPress={()=>this.props.navigation.navigate('QrScan')}
        style={{backgroundColor:'#FE3232', flexDirection:'row', padding:10, borderRadius:10, alignItems:'center'}}>
          <FontAwesome
              name="qrcode"
              color='white'
              size={35}
            />
            <Text style={{color:'white', fontSize:30, padding:5, fontWeight:'bold'}}>QR SCAN</Text>
        </TouchableOpacity>

      </View>


    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  profile:{
    justifyContent: 'center',
    width: 100,
    height: 100,
    padding:10,
    borderRadius: 10
  },
});