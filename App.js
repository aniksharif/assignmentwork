
import React from 'react';
import { Platform, StyleSheet, Text, View, ActivityIndicator, StatusBar } from 'react-native';
import {createStackNavigator,createBottomTabNavigator,createSwitchNavigator } from 'react-navigation';
import { Icon } from 'react-native-elements'
import Entypo from 'react-native-vector-icons/Entypo'
import Login from './app/screens/Login'
import Home from '@home/Home'
import QrScan from '@home/QrScan'
import Payment from '@home/Payment'
import Recharge from '@home/Recharge'
import Success from '@home/Success'
import User from './app/screens/User'
import Alarm from './app/screens/Alarm'
import Timer from './app/screens/Timer'
import Settings from './app/screens/Settings'

console.disableYellowBox = true;

const LoginStack = createStackNavigator({
  Login:{
    screen:Login, navigationOptions: { header: null }
  }
})
const HomeStack=createStackNavigator(

    {
        Home: Home,
        QrScan:QrScan,
        Payment:Payment,
        Recharge:Recharge,
        Success:Success
    },
    {
      
      navigationOptions: ({navigation}) => ({
        headerLeft: () => (
          <View >
            <Entypo name="chevron-left" size={30} color={'#DA3232'} onPress={() => navigation.goBack()} />
          </View>
        ),
        headerStyle: {
          backgroundColor: '#D0D0D0',
        },
      })
    }

);
const AlarmStack=createStackNavigator(

  {
      Alarm: Alarm,
      
  },
  {
    
    navigationOptions: ({navigation}) => ({
      headerLeft: () => (
        <View >
          <Entypo name="chevron-left" size={30} color={'#DA3232'} onPress={() => navigation.goBack()} />
        </View>
      ),
      headerStyle: {
        backgroundColor: '#D0D0D0',
      },
    })
  }

);
const SettingsStack=createStackNavigator(

  {
    Settings: Settings,
      
  },
  {
    
    navigationOptions: ({navigation}) => ({
      headerLeft: () => (
        <View >
          <Entypo name="chevron-left" size={30} color={'#DA3232'} onPress={() => navigation.goBack()} />
        </View>
      ),
      headerStyle: {
        backgroundColor: '#D0D0D0',
      },
    })
  }

);
const TimerStack=createStackNavigator(

  {
    Timer: Timer,
      
  },
  {
    
    navigationOptions: ({navigation}) => ({
      headerLeft: () => (
        <View >
          <Entypo name="chevron-left" size={30} color={'#DA3232'} onPress={() => navigation.goBack()} />
        </View>
      ),
      headerStyle: {
        backgroundColor: '#D0D0D0',
      },
    })
  }

);
const UserStack=createStackNavigator(

  {
    User: User,
      
  },
  {
    
    navigationOptions: ({navigation}) => ({
      headerLeft: () => (
        <View >
          <Entypo name="chevron-left" size={30} color={'#DA3232'} onPress={() => navigation.goBack()} />
        </View>
      ),
      headerStyle: {
        backgroundColor: '#D0D0D0',
      },
    })
  }

);
const BottomStack=createBottomTabNavigator(
  {
    Home: HomeStack,
    User:UserStack,
    Alarm:AlarmStack,
    Timer:TimerStack,
    Settings:SettingsStack,
  },
  {
    navigationOptions: ({ navigation }) => ({
      
      tabBarOnPress: (scene) => {
        if (typeof scene.navigation.replace === undefined || !scene.navigation.replace) {
            scene.navigation.navigate(scene.navigation.state.routeName);
        }
        else {

            scene.navigation.replace(scene.navigation.state.routeName);
        }

    },
      tabBarIcon: ({ tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
            iconName = "home";
            typeName = "entypo";
        }
        else if (routeName === 'User') {
            iconName = "person";
            typeName = "octicon";
        }
        else if (routeName === 'Alarm') {
            iconName = "bell";
            typeName = 'font-awesome'
        }
        else if (routeName === 'Timer') {
            iconName = "restore-clock";
            typeName = "material-community"
        }
        else if (routeName === 'Settings') {
            iconName = "gear";
            typeName = "font-awesome"
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Icon name={iconName} size={25} type={typeName} color={tintColor} />;
    },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: 'white',
      showLabel:false,
      style: {
        backgroundColor: '#FD3131',
      },
    },
   
  }
)

const Navigation=createSwitchNavigator(
  {
    Login:LoginStack,
    Bottom:BottomStack,
    
  }
)


export default Navigation;